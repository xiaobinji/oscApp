var express = require('express');
var httpHelper = require('../utils/httpHelper')
var xml2js = require('xml2js');
var parseString = xml2js.parseString;

var OSCAPI = require('../utils/OSCAPI')

var router = express.Router();

router.get('/:id',function(req,res,next){
  var url=[OSCAPI.software_detail,'id='+req.params.id].join('?');
  var result={};
  httpHelper.get(url,30000,function(err,xml){
    if(err){
      console.log(err);
      next();
    }
    parseString(xml, {explicitArray:false,ignoreAttrs:true},function (err, data) {
        console.log(JSON.stringify(data));
        result=data;
        res.json(result);
    });

  })
})

module.exports = router;
