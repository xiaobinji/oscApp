var express = require('express');
var httpHelper = require('../utils/httpHelper')
var xml2js = require('xml2js');
var parseString = xml2js.parseString;

var OSCAPI = require('../utils/OSCAPI')

var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/rock_rock',function(req,res,next){
  var url = OSCAPI.rock_rock;
  httpHelper.get(url,30000,function(err,xml){
    if(err){
        console.log(err);
        next()
    }
    //console.log(data);
    parseString(xml, {explicitArray:false,ignoreAttrs:true},function (err, data) {
        //console.log(JSON.stringify(data));
        result=data;
        res.json(result);
    });
 
  })
})

module.exports = router;
