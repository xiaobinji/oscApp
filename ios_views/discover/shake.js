/**
* 摇一摇
*/
'use strict'
import Util from "../common/Util"
import Api from "../common/Api"

import BlogDetail from "../user/blog_detail"
import NewsDetail from "../news/news_detail"
import SoftwareDetail from "../user/software_detail"

import RNShakeEventIOS from 'react-native-shake-event-ios';

var React = require('react-native');
var {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Text,
}=React;

export default class Shake extends React.Component{
  constructor(props){
    super(props)
    this.state={
      show:false,
      data:{}
    }
  }

  componentWillMount() {
    RNShakeEventIOS.addEventListener('shake', () => {
      console.log('Device shake!');
      Util.get(Api.rock_rock,(data)=>{
        console.log('shakeing:'+JSON.stringify(data));
        this.setState({
          show:true,
          data:data.oschina
        })
      });
    });
  }

  componentWillUnmount() {
    RNShakeEventIOS.removeEventListener('shake');
  }

  _loadDetailPage(){
    const data=this.state.data;
    const type=parseInt(data.randomtype);
    if(type===3){
      this.props.navigator.push({
        component:<BlogDetail objid={data.id}/>,
        title:'博客详情'
      })
    }else if(type===4){
      this.props.navigator.push({
        component:<NewsDetail id={data.id}/>,
        title:'资讯详情'
      })
    }else if(type===1){
      this.props.navigator.push({
        component:<SoftwareDetail objid={data.id}/>,
        title:'软件详情'
      })
    }
  }

  render(){
    return (
      <View style={{flex:1}}>
        {
          this.state.show?
          <ShakeItem row={this.state.data} onPress={this._loadDetailPage.bind(this)}/>
          :
          <View style={styles.container}>
            <Image
              source={require('image!shaking')}>
            </Image>
          </View>
        }
      </View>

    );
  }
}

class ShakeItem extends React.Component{
  constructor(props){
    super(props)
  }

  render(){
    const {row}=this.props;

    return (
      <TouchableOpacity  {...this.props}>
        <View style={styles.item}>
          <Text style={styles.title}>{row.title}</Text>
        </View>
      </TouchableOpacity>

    );
  }
}

var styles=StyleSheet.create({
  container:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
  },
  item:{
    minHeight:40,
    borderBottomWidth:Util.pixel,
    borderColor:'#ccc',
    paddingBottom:10,
    paddingTop:10,
    justifyContent:'center'
  },
  title:{
    paddingLeft:10,
  }
})
