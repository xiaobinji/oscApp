
var BaseURL='http://localhost:3000'
module.exports={
  news_list:BaseURL+'/news/list',
  news_detail:BaseURL+'/news',

  login:BaseURL+'/user/login',
  my_information:BaseURL+'/user/my_information',
  favorite_list:BaseURL+'/favorite/favorite_list',
  find_user:BaseURL+'/user/find_user',

  blog_list:BaseURL+'/blog/list',
  blog_detail:BaseURL+'/blog/blog_detail',

  software_detail:BaseURL+'/software/software_detail',

  tweet_list:BaseURL+'/tweet/tweet_list',

  rock_rock:BaseURL+'/rock_rock',
}
